const buildYScale = (allData) => {
    let amt = allData.map((row) => parseFloat(row.total_amount));
    let max = Math.max.apply(Math, amt);;

    return d3.scaleLinear()
        .domain([0, max]) // dropoff
        .range([0, 800]); //pixel location to scale between
}

/**
 * 
 * tpep_dropoff_datetime: "2019-01-01 01:18:59"
​​​
tpep_pickup_datetime: "2019-01-01 00:59:47"
 */
const buildXScale = (allData) => {
    let distances = allData.map((row) => parseFloat(row.trip_distance));
    let max = Math.max.apply(Math, distances);

    return d3.scaleLinear()
        .domain([0, max]) //trip_distacnce
        .range([0, 800])
}

const buildSizeScale = (allData) => {
    let tip = allData.map((row) => parseFloat(row.tip_amount));
    let max = Math.max.apply(Math, tip);
    let minRadius = 3;
    let maxRadius = 13;

    return d3.scaleLinear()
        .domain([0, max]) //size based off data point
        .range([minRadius, maxRadius])
}
const sanitizeData = (csvArr) => {

    let firstRow = csvArr.splice(0, 1);
    let sanitized = [];
    let headers = firstRow[0]


    headers.splice(-1, 1)

    csvArr.forEach((row) => {
        let cleanRow = {};
        headers.forEach((header, headerIndex) => {
            cleanRow[header] = row[headerIndex]
        })
        if (parseFloat(cleanRow.trip_distance) > 2 && parseFloat(cleanRow.trip_distance) < 10
            && parseFloat(cleanRow.total_amount) > 10
            // && parseFloat(cleanRow.total_amount) < 40
        ) {
            sanitized.push(cleanRow);
        }

    });

    return sanitized;
};

const performSimulation = (data) => {
    let sanitizedData = sanitizeData(data);

    let yScale = buildYScale(sanitizedData);
    let xScale = buildXScale(sanitizedData);
    let sizeScale = buildSizeScale(sanitizedData);
    let simulation = d3.forceSimulation(sanitizedData)
        .force('y', d3.forceY((row) => {
            return yScale(parseFloat(row.total_amount))
        }))
        .force('x', d3.forceX((row) => {
            return xScale(parseFloat(row.trip_distance));
        }))
        .force('collide', d3.forceCollide((row) => {
            let padding = 0.5;
            return sizeScale(parseFloat(row.tip_amount));
        }).strength(1).iterations(2));

    simulation.tick(300);

    return sanitizedData;
}