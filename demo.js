import CSV from './csv.js'
import * as utils from './utils.js';

function generateWithWorker(){
    startLoad();
    let chartWorker = new Worker('chart-worker.js');
    let parsed;
    // Fetch our data
    fetch('./yellow_tripdata_2019-01.csv').then((response) => {
        return response.text();
    }).then((text) => {
        parsed = CSV.parse(text);
        chartWorker.postMessage(parsed);
    })

    chartWorker.onmessage = function (e) {
        let chartData = e.data;
        document.querySelector('#loader').style.display = 'none';
        document.querySelector('#chart').style.display = 'block'
        utils.renderXAxis(chartData);
        utils.renderYAxis(chartData);
        utils.renderBubbles(chartData);
    }
}
function startLoad(){
    document.querySelector('#chart').style.display = 'none';
    document.querySelector('#loader').style.display = 'block'
}
function generateWithoutWorker(){
    startLoad();
    let parsed;
    // Fetch our data
    fetch('./yellow_tripdata_2019-01.csv').then((response) => {
        return response.text();
    }).then((text) => {
        parsed = CSV.parse(text);
    
        let chartData = performSimulation(parsed);
        document.querySelector('#loader').style.display = 'none';
        document.querySelector('#chart').style.display = 'block'
        utils.renderXAxis(chartData);
        utils.renderYAxis(chartData);
        utils.renderBubbles(chartData);
    })
}

window.demo = {
    generateWithWorker,
    generateWithoutWorker
}