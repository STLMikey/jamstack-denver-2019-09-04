onmessage = function (e) {
    importScripts("https://d3js.org/d3.v5.min.js");
    importScripts("chart-helpers.js")

    let chartData = performSimulation(e.data)

    postMessage(chartData);
}