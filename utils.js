export function renderXAxis( allData ){
        let xElement = d3.select('#x-axis').attr('width', 800).attr('class', 'x-axis')
        let xAxis = d3.axisBottom()
        .scale(buildXScale(allData))
        xElement.call(xAxis);
};

export function renderYAxis(allData){
        let yElement = d3.select('#y-axis').attr('height', 800).attr('width', 800).attr('class', 'y-axis')
        let yAxis = d3.axisRight()
            .scale(buildYScale(allData))
        yElement.call(yAxis);
}

export function renderBubbles(allData, tooltip){
    d3.select('#bubbles').selectAll('dot').data(allData)
    .enter()
    .append('circle')
    .attr('cx', function(d){
        return d.x;
    })
    .attr('cy', function (d) {
        return d.y;
    })
    .attr("class", function (row) {
        return `passenger-${row.passenger_count}`
    })
    .attr('r', function(row){
        let scale = buildSizeScale(allData);
        return scale( parseFloat(row.tip_amount))
    })
    .on("mouseover", function (d) {
        document.getElementById('log').innerHTML = `
        <div> Passenger Count: ${d.passenger_count}</div>
        <div> Total Amount: ${d.total_amount}</div>
        <div> Tip Amount: ${d.tip_amount}</div>
        <div> Trip Distance: ${d.trip_distance}</div>
        <div> Trip Time (minutes): ${(( new Date(d.tpep_dropoff_datetime) - new Date(d.tpep_pickup_datetime) ) / 1000 / 60).toLocaleString() }</div>
        `;
    })
    .on("mouseout", function (d) {
        document.getElementById('log').innerHTML = 'Hover something!';
    });
       
}
export function buildYScale(allData){
    let amt = allData.map((row) => parseFloat(row.total_amount));
    let max = Math.max.apply(Math, amt);;

    return d3.scaleLinear()
        .domain([0, max]) // dropoff
        .range([0, 800]); //pixel location to scale between
}
export function buildXScale(allData){
        let distances = allData.map((row) => parseFloat(row.trip_distance));
        let max = Math.max.apply(Math, distances);

        return d3.scaleLinear()
            .domain([0, max]) //trip_distacnce
            .range([0, 800])
    }
export function buildSizeScale(allData){
    let tip = allData.map((row) => parseFloat(row.tip_amount));
    let max = Math.max.apply(Math, tip);
    let minRadius = 3;
    let maxRadius = 13;

    return d3.scaleLinear()
        .domain([0, max]) //size based off data point
        .range([minRadius, maxRadius])
}
